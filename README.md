# Atmo Workspace with PostgreSQL

## About this project

This project is a sandbox with all that you need to start playing with **Atmo** and **PostgreSQL** right now without installing anything. So, **to get the benefit of that, you have to open this project with [Gitpod](https://www.gitpod.io/)**.

👋 You can see this project as a **"freestanding workshop"** (you can do it on your own).

### 🤚 To begin, click on the **Gitpod** button: [![Open in GitPod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/k33g_org/discovering-atmo/atmo-postgresql)

> To start the database and the Atmo server, just run `docker-compose up`

## What is Atmo?

**[Atmo](https://github.com/suborbital/atmo)** is a project from [Suborbital](https://suborbital.dev/). 

To say it short, Atmo is a toolchain allowing writing and deploying wasm microservices smoothly.
With Atmo, you can write microservices in Rust, Swift, and AssemblyScript without worrying about complicated things, and you only have to care about your source code:

- No fight with wasm and string data type
- No complex toolchain to install
- Easy deployment with Docker (and then it becomes trivial to deploy wasm microservices on Kubernetes)
- ...

